package com.Sprites2Css;

import com.master.util.Logger;
import com.master.util.SearchableString;
import com.master.util.StringElement;

import java.nio.MappedByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.channels.FileChannel;
import java.io.RandomAccessFile;
import java.io.File;

public final class Main{

	private static final Logger LOGGER = new Logger("Sprites2Css.log",true,false,false,false);
	private static final Logger CSS_FILE = new Logger("sprites.css",false,false,false,false);
	private static final Logger JSON_FILE = new Logger("sprites.json",false,false,false,false);

	public static final void main(final String[] ARGS) throws Exception{
		if(ARGS.length != 2){
			LOGGER.writeLine("Usage: java -jar Sprites2Css.jar [TextFile] [imageURL]");
			LOGGER.writeLine("Example: java -jar Sprites2Css.jar sprites.txt http://domain.com/image.png");
			LOGGER.writeLine("");
			LOGGER.writeLine("Sprites2Css creates css and json file of sprite packed images from http://spritesheetpacker.codeplex.com/");
			LOGGER.writeLine("");
			return;
		}
		new Main(ARGS[0],ARGS[1]);
	}
	
	public Main(final String FILE_NAME, final String URL) throws Exception{
		MappedByteBuffer buffer = null;
		try{
			final Path PATH = Paths.get(FILE_NAME);
			if(Files.exists(PATH)){
				LOGGER.writeLine("[Found] "+FILE_NAME);
				final File FILE = new File(FILE_NAME);
				final RandomAccessFile ACCESS_FILE = new RandomAccessFile(FILE, "r");
				buffer = ACCESS_FILE.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, FILE.length()).load();
				final SearchableString STRING_SEARCH = new SearchableString(buffer);
				boolean firstRun = true;
				int total = 0;
				try{
					do{
						if(firstRun){
							final String VALUES = STRING_SEARCH.getString("\n");
							final String[] EQUALS_SPLIT = VALUES.split("=");
							final String[] VALUES_SPLIT = EQUALS_SPLIT[1].split(" ");
							firstRun = false;
							total++;
						}else{
							final StringElement ELEMENT = STRING_SEARCH.findIndex("\n");
							if(ELEMENT != null){
								final String VALUES = STRING_SEARCH.getString("\n");
								final String[] EQUALS_SPLIT = VALUES.split("=");
								final String[] VALUES_SPLIT = EQUALS_SPLIT[1].split(" ");
								total++;
							}else{
								break;
							}
						}
					}while(true);
				}catch(final Exception ex){}
				LOGGER.writeLine("[Found] "+total+" Images");
				firstRun = true;
				int tmp = 0;
				STRING_SEARCH.position(0);
				try{
					do{
						if(firstRun){
							final String VALUES = STRING_SEARCH.getString("\n");
							final String[] EQUALS_SPLIT = VALUES.split("=");
							final String[] VALUES_SPLIT = EQUALS_SPLIT[1].split(" ");
							CSS_FILE.write(String.format(".%1$s-Image{background: url('%2$s') -%3$spx -%4$spx; height: %5$spx; width: %6$spx; display: inline-block;}",EQUALS_SPLIT[0].trim(),URL,VALUES_SPLIT[1],VALUES_SPLIT[2],VALUES_SPLIT[3],VALUES_SPLIT[4]).replace("\r","").replace("\n",""));
							JSON_FILE.write(String.format("[{\"ID\": %1$s, \"X\": %2$s, \"Y\": %3$s},",EQUALS_SPLIT[0].trim(),VALUES_SPLIT[1],VALUES_SPLIT[2]));
							firstRun = false;
							tmp++;
						}else{
							final StringElement ELEMENT = STRING_SEARCH.findIndex("\n");
							if(ELEMENT != null){
								final String VALUES = STRING_SEARCH.getString("\n");
								final String[] EQUALS_SPLIT = VALUES.split("=");
								final String[] VALUES_SPLIT = EQUALS_SPLIT[1].split(" ");
								CSS_FILE.write(String.format(" .%1$s-Image{background: url('%2$s') -%3$spx -%4$spx; height: %5$spx; width: %6$spx; display: inline-block;}",EQUALS_SPLIT[0].trim(),URL,VALUES_SPLIT[1],VALUES_SPLIT[2],VALUES_SPLIT[3],VALUES_SPLIT[4]).replace("\r","").replace("\n",""));
								if(++tmp == total){
									JSON_FILE.write(String.format("{\"ID\": %1$s, \"X\": %2$s, \"Y\": %3$s}]",EQUALS_SPLIT[0].trim(),VALUES_SPLIT[1],VALUES_SPLIT[2]));
								}else{
									JSON_FILE.write(String.format("{\"ID\": %1$s, \"X\": %2$s, \"Y\": %3$s},",EQUALS_SPLIT[0].trim(),VALUES_SPLIT[1],VALUES_SPLIT[2]));
								}
							}else{
								break;
							}
						}
					}while(true);
				}catch(final Exception ex){}
			}else{
				LOGGER.writeLine("No Such File "+FILE_NAME);
			}
		}catch(final Exception e){}
	}

}