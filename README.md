Spirtes2Css
========================================
A tool that takes sprites packed with 
[spritesheetpacker.codeplex.com](http://spritesheetpacker.codeplex.com/ 
"Homepage") and makes a CSS and JSON file to reference the sprites.   
    
Usage: java -jar Sprites2Css.jar [TextFile] [imageURL]   
Example: java -jar Sprites2Css.jar sprites.txt 
http://domain.com/image.png   

Prerequisites
-------------
1. Apache Ant
2. Master API
 > 
[https://bitbucket.org/subasteve/master-api](https://bitbucket.org/subasteve/master-api 
"Master API")

Compile
-------------
ant

Author
-------------
Stephen Hineline  
[stephenhineline.com](http://stephenhineline.com/ "Homepage")
